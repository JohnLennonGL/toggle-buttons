package com.jlngls.togglebuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioButton masculino,feminino;
    private RadioGroup radioGroup;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        masculino = findViewById(R.id.radioButtonMasc);
        feminino = findViewById(R.id.radioButtonFem);
        radioGroup = findViewById(R.id.radioGroup);
        resultado = findViewById(R.id.resultado);

        radioGroup();
    }

    public void radioGroup(){
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(masculino.isChecked()){
                    resultado.setText("Masculino");
                }
                else if(feminino.isChecked()) {
                    resultado.setText("Feminino");
                }
            }
        });
    }
}
